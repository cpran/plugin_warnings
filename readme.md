warnings
========

Description
-----------

This plugin defines a method for printing warning messages. Once a warning has been flagged, further warnings of the same type are not issued again.

Requirements
------------

* [selection](https://gitlab.com/cpran/plugin_selection)